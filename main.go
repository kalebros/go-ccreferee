package main

import (
	_ "go-ccreferee/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

