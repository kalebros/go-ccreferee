# GO-CCREFEREE

 Servidor basico de Torneos para CCReferee, programado en Go

## Apuntes de testeo

Para poder testear correctamente el metodo POST para recibir datos JSON,
utilizar este código:

 > curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"Nombre":"JLA 2016","Tipo":"Catan"}' http://localhost:8080/api/v1/torneo
