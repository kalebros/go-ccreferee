package routers

import (
	"go-ccreferee/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/api/v1",
		beego.NSNamespace("/torneo",
			beego.NSInclude(
				&controllers.APITorneoController{},
			),
		))
	beego.AddNamespace(ns)

}
