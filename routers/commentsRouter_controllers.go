package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"] = append(beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"] = append(beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"] = append(beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"] = append(beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

	beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"] = append(beego.GlobalControllerRouter["go-ccreferee/controllers:APITorneoController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"delete"},
			Params: nil})

}
