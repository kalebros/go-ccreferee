package controllers

import (
	"encoding/json"
	"go-ccreferee/models"

	"github.com/astaxie/beego"
)

type APITorneoController struct {
	beego.Controller
}

// @Title CreateTorneo
// @Description create torneo
// @Param	body		body 	models.Torneo	true		"body for torneo content"
// @Success 200 {int} models.Torneo.Id
// @Failure 403 body is empty
// @router / [post]
func (t *APITorneoController) Post() {
	var torneo models.Torneo
	//	fmt.Println(t.Ctx.Input.RequestBody)
	json.Unmarshal(t.Ctx.Input.RequestBody, &torneo)
	uid := models.AddTorneo(torneo)
	t.Data["json"] = map[string]string{"uid": uid}
	t.ServeJSON()
}

// @Title GetAll
// @Description get all Torneo
// @Success 200 {object} models.Torneo
// @router / [get]
func (t *APITorneoController) GetAll() {
	torneos := models.GetAllTorneos()
	t.Data["json"] = torneos
	t.ServeJSON()
}

// @Title Get
// @Description get torneo by uid
// @Param	uid		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Torneo
// @Failure 403 :uid is empty
// @router /:uid [get]
func (t *APITorneoController) Get() {
	uid := t.GetString(":uid")
	if uid != "" {
		torneo, err := models.GetTorneo(uid)
		if err != nil {
			t.Data["json"] = err.Error()
		} else {
			t.Data["json"] = torneo
		}
	}
	t.ServeJSON()
}

// @Title Update
// @Description update the torneo
// @Param	uid		path 	string	true		"The uid you want to update"
// @Param	body		body 	models.Torneo	true		"body for user content"
// @Success 200 {object} models.Torneo
// @Failure 403 :uid is not int
// @router /:uid [put]
func (t *APITorneoController) Put() {
	uid := t.GetString(":uid")
	if uid != "" {
		var torneo models.Torneo
		json.Unmarshal(t.Ctx.Input.RequestBody, &torneo)
		tt, err := models.UpdateTorneo(uid, &torneo)
		if err != nil {
			t.Data["json"] = err.Error()
		} else {
			t.Data["json"] = tt
		}
	}
	t.ServeJSON()
}

// @Title Delete
// @Description delete the torneo
// @Param	uid		path 	string	true		"The uid you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 uid is empty
// @router /:uid [delete]
func (t *APITorneoController) Delete() {
	uid := t.GetString(":uid")
	models.DeleteTorneo(uid)
	t.Data["json"] = "delete success!"
	t.ServeJSON()
}
