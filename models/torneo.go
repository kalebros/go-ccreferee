package models

import (
	"errors"
	"strconv"
	"time"
)

var (
	TorneoList map[string]*Torneo
)

func init() {
	TorneoList = make(map[string]*Torneo)
	t := Torneo{"torneo_1111", "Torneo de Carcassonne", "Carcassonne"}
	TorneoList["torneo_1111"] = &t
}

type Torneo struct {
	Id     string
	Nombre string
	Tipo   string
}

func AddTorneo(t Torneo) string {
	t.Id = "torneo_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	TorneoList[t.Id] = &t
	return t.Id
}

func GetTorneo(uid string) (t *Torneo, err error) {
	if t, ok := TorneoList[uid]; ok {
		return t, nil
	}
	return nil, errors.New("Torneo not exists")
}

func GetAllTorneos() map[string]*Torneo {
	return TorneoList
}

func UpdateTorneo(uid string, tt *Torneo) (a *Torneo, err error) {
	if t, ok := TorneoList[uid]; ok {
		if tt.Nombre != "" {
			t.Nombre = tt.Nombre
		}
		if tt.Tipo != "" {
			t.Tipo = tt.Tipo
		}
		return t, nil
	}
	return nil, errors.New("Torneo Not Exist")
}

func DeleteTorneo(uid string) {
	delete(TorneoList, uid)
}
